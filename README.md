# AI_Employability_prediction

This project extracts information from the resumes which are subsequently used to predict the employability of an individual. Resumes are stored on minio server.

# Features to extract

Name, email, skills, experience, GPA

# Dependencies

## Uploading to minio server file: To connect to Minio Server

Run the following command on command line:


```
wget https://dl.minio.io/server/minio/release/linux-amd64/minio
chmod +x minio
./minio server datastore
```  

./minio server datastore, here datastore is the name of the local repository i.e. a folder named datastore will be createdin the current directory

Use the endpoint without https, access key and secret key and use it to initialize minio client

## Extracting information file : Install spacy
2. !pip install spacy --user